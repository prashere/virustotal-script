#!/usr/bin/env python3

'''
Python script to talk to virustotal consuming it's
API. This piece of software is a free software.
Pleae read through LICENSE file to know more.

Author: Prasanna Venkadesh
License: GNU GPL v3
'''


import requests
import webbrowser
from time import sleep

av_list = []
base_url = "https://www.virustotal.com/vtapi/v2/file/report"
headers = {
        "Accept-Encoding": "gzip, deflate",
        "User-Agent": "gzip, python client"
        }
params = {}


def get_api_key():
    try:
        with open('apikey.txt', 'r') as apikey_file:
            return apikey_file.readlines()[0].strip()
    except FileNotFoundError as fe:
        print ("ERROR: 'apikey.txt' file not found")
        exit()

def prepare_av_list():
    try:
        with open('antivirus-list.txt', 'r') as av_file:
            for antivirus in av_file.readlines():
                av_list.append(antivirus.strip())
    except FileNotFoundError as fe:
        print ("ERROR: 'antivirus-list.txt' file not found")
        exit()

def start():
    counter = 1
    try:
        with open('checksums.txt', 'r') as sha_file:
            with open('output.html', 'w') as html_file:

                line = "<html>\n<head>"
                line = line + "\n<link rel='stylesheet' href='./static/css/skeleton.css'/>"
                line = line + "\n<link rel='stylesheet' href='./static/css/style.css'/>\n</head>"
                line =  line + "\n<body>\n<div class='container'>\n<div class='row'>\n<div class='tweleve columns'>"
                line = line + "\n<table>\n<thead>\n<tr>\n<th>Hash</th>"
                for av in av_list:
                    line = line + "\n<th>%s</th>" % av
                line = line + "\n<th>Permalink</th>\n</tr>\n</thead>\n<tbody>"
                html_file.writelines(line)

                for checksum in sha_file.readlines():
                    hash_value = checksum.strip()
                    new_line = "\n<tr>\n\t<td>%s</td>" % hash_value
                    params['resource'] = hash_value

                    if (counter % 5) == 0:
                        print ("=" * 10)
                        print ("WAITING: 1 minute due to 4 requests/min limit")
                        print ("=" * 10)
                        sleep(60)

                    print ("FETCHING: for %s" % hash_value)
                    response = requests.get(base_url, params=params, headers=headers)
                    if response.status_code == 200:
                        json_response = response.json()
                        response_code = json_response.get('response_code')
                        if response_code == 1:
                            for av in av_list:
                                try:
                                    new_line = new_line + "\n\t<td>%s</td>" % json_response.get('scans').get(av).get('result')
                                except AttributeError as ae:
                                    new_line = new_line + "\n\t<td>None</td>"
                            new_line = new_line + "\n\t<td>%s</td>\n</tr>" % json_response.get('permalink')

                        elif response_code == 0:
                            new_line = new_line + "\n\t<td colspan='4'>No info about this hash</td>\n</tr>"

                    elif response.status_code == 204:
                        print ("4 requests/minute exceed. Wait for 1 minute and then try again")
                        exit()

                    counter += 1
                    html_file.writelines(new_line)

                html_file.writelines("\n</tbody></table>\n</body>\n</html>")
                print ("\n\n")
                print ("*" * 30)
                print ("DONE: output.html created")
                print ("*" * 30)
                return 0

    except FileNotFoundError as fe:
        print ("ERROR: 'checksum.txt' file not found")
        exit()


if __name__ == "__main__":
    # preparation before processing
    prepare_av_list()
    params['apikey'] = get_api_key()

    # start processing
    start()

    # open the output file in default browser
    webbrowser.open_new_tab('output.html')
