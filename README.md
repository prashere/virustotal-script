Based on a request from my friend, I wrote this script for their use to hit VIRUSTOTAL API using hash values to get response back and the output is written to a html file.

### Requirements

Make sure you have **Python 3** and **Pip** installed before proceeding further.

**GNU/Linux**

1. Open this downloaded directory in `terminal`.
2. Execute `sudo pip install -r requirements.txt`

**Windows**

1. Open this downloaded folder in `command prompt`.
2. Execute `pip install -r requirements.txt`


### Setup

These steps are common for both operating systems.

1. 3 files are necessary for the script to run successfully.
2. `checksums.txt` (virustotal accepts either files (or) urls (or) hashes, i.e checksums)
3. `apikey.txt` (visit virustotal.com, signup and find your **public api key**. copy it and paste it in this file)
4. `antivirus-list.txt` (list of anti-virus tools to filer in virustotal result. Note this is case sensitive. If you are unsure, check virustotal website for names.)

### Usage

From terminal or command prompt execute the following once you have properly setup.

        python program.py

You can see logs while the script is performing. Virustotal places a limit of 4 requests/minute on their free public API, therefore the script will pause for 1 minute for every 4 requests made.
Leave the script running until it tells you that it is DONE. By default when the script is over it would automatically open the output html file in your default browser for you to view.

In case of any error or failure, create a **issue** here.
